package net.myotherworld.Ping;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class PingScheduler extends BukkitRunnable
{
	public Map<String, Integer> PingPlayer = new HashMap<String, Integer>();
	public List<String> Data = new ArrayList<String>();
	public Ping plugin;
	public BukkitTask PingTimer;
	
	public PingScheduler(Ping pl)
	{
		this.plugin = pl;
	}
	@Override
	public void run() 
	{
		for(final Player player : Bukkit.getOnlinePlayers())
		{
			if(!plugin.config.enabledWorlds.contains(player.getWorld().getName()))
			{		
				PingTimer = Bukkit.getScheduler().runTaskTimer(this.plugin, new Runnable() 
				{
					public void run()
				    {		 
						int ping = Utils.getPlayerPing(player);	
				    	CheckPing(ping, player); 
				    }
				}, plugin.config.Timer, plugin.config.Timer);
			}
		}
	}
	public void Warring(int ping, Player player)
	{
        if((ping > plugin.config.Max))
    	{
			List<String> list = plugin.config.WarringCommands;
				
			for(String command : list)
			{
				command = command.replace("@p", player.getName());
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);			
			}
    	}	   	
	}
	public void CheckPing(int ping, Player player)
	{		
	    if(ping > plugin.config.Max)
	    {	    	   	   	    	
	    	if(!PingPlayer.containsKey(player.getName())) 
	    	{         
	    		PingPlayer.put(player.getName(), 1);
	    		Warring(ping, player);
	    	}
	    	else
	    	{
	    		PingPlayer.put(player.getName(), PingPlayer.get(player.getName()) + 1);
	    		Warring(ping, player);
	    	}
	    	if(PingPlayer.get(player.getName()) == plugin.config.Loop) 
	    	{
				List<String> list = plugin.config.Commands;
				
				for(String command : list)
				{
					command = command.replace("@p", player.getName());
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
					SavePlayer(player);
					
				}			
	    		PingPlayer.remove(player.getName());
	    	}
	    }
	}
	public void SavePlayer(Player player)
	{	
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("[dd-MM-yyyy] [HH:mm:ss]");
		
		if(plugin.fileManager.config.getString("User." + player.getName()) == null)
        {
			Data = Arrays.asList(format.format(now));			 
			plugin.fileManager.config.set("User." + player.getName() , Data);    
        }
        else
        {
        	Data = plugin.fileManager.config.getStringList("User." + player.getName());
        	Data.add(format.format(now));
        	plugin.fileManager.config.set("User." + player.getName(), Data);     	
        }
		try
		{
			plugin.fileManager.config.save(plugin.fileManager.configFile);		
		} 
		catch (IOException io) 
		{
			Bukkit.getServer().getLogger().severe("Blad podczas zapisu pliku User!");
		} 
	}
}

package net.myotherworld.Ping;

import net.myotherworld.Ping.Commands.AdminCommand;
import net.myotherworld.Ping.Commands.PingCommand;
import net.myotherworld.Ping.Data.ConfigData;
import net.myotherworld.Ping.Data.MessageData;
import net.myotherworld.Ping.Managers.FileManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class Ping extends JavaPlugin
{
	public FileManager fileManager;
	public MessageData message;
	public ConfigData config;
	public BukkitTask Scheduler;
	public PingScheduler Task;
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEnable() 
    { 
    	config();
    	Commands();
    	Scheduler();
    }
	public void Commands()
	{
    	getCommand("Ping").setExecutor( new PingCommand(this) );
    	getCommand("MowPing").setExecutor( new AdminCommand(this) );
	}
	public void config()
    {
		fileManager = new FileManager(this);
		config = new ConfigData(fileManager.loadConfig());
		message = new MessageData(fileManager.loadMessages());
    }
    @SuppressWarnings("deprecation")
    public void Scheduler() 
    {	
		Scheduler = getServer().getScheduler().runTask(this, new PingScheduler(this));
	}

}

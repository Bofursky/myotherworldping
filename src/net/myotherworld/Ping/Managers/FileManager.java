package net.myotherworld.Ping.Managers;

import java.io.File;
import java.io.IOException;

import net.myotherworld.Ping.Ping;

import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager
{
	private Ping plugin;
	public YamlConfiguration config;
	public File configFile;
	public FileManager(Ping plugin)
	{
		this.plugin = plugin;
		loadUser();
	}  
	public YamlConfiguration loadConfig()
	{
		try 
	    {
			if (!plugin.getDataFolder().exists())
	        {
				plugin.getDataFolder().mkdirs();
	        }
	        if (!new File(plugin.getDataFolder(), "config.yml").exists())
	        {
	        	plugin.saveResource("config.yml", false);
	        }
	        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));     
	    } 
		catch (Exception ex) 
	    {
	        plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	        plugin.getServer().getPluginManager().disablePlugin(plugin);
	        return null;
	    }
	}	
	public YamlConfiguration loadMessages()
	{
		try 
	    {
			if (!plugin.getDataFolder().exists())
	        {
				plugin.getDataFolder().mkdirs();
	        }
	        if (!new File(plugin.getDataFolder(), "messages.yml").exists())
	        {
	        	plugin.saveResource("messages.yml", false);
	        }
	        return YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "messages.yml"));     
	    } 
		catch (Exception ex) 
	    {
	        plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
	        plugin.getServer().getPluginManager().disablePlugin(plugin);
	        return null;
	    }
	}
	public void loadUser()
	{
		try
		{
			if (!this.plugin.getDataFolder().exists()) 
			{
				this.plugin.getDataFolder().mkdirs();
			}
			if (!new File(this.plugin.getDataFolder(), "user.yml").exists()) 
			{
				this.plugin.saveResource("user.yml", false);
			}
			this.configFile = new File(this.plugin.getDataFolder(), "user.yml");
			this.config = YamlConfiguration.loadConfiguration(this.configFile);
		}
		catch (Exception ex)
		{
			this.plugin.getLogger().severe("Blad podczas wczytywania pliku User!");
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
			return;
		}
	}
	public void saveUser(String patch, Object value)
	{
		this.config.set(patch, value);
		try
		{
			this.config.save(this.configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	  
	}
}

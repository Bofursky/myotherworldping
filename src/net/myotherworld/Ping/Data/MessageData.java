package net.myotherworld.Ping.Data;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class MessageData
{
	public String Permissions;
	public String Prefix;
	public String Check;
	public String PlayerInvalid;
	public String Ping;
	public List<String> Admins;
	public String NoCommands;
	
	public MessageData(YamlConfiguration message) 
	{
		Prefix = ChatColor.translateAlternateColorCodes('&',message.getString("Prefix", "Ping"));
		
		Ping = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Ping", "Aktualny ping gracza:" ));
		Check = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Check", "Uzyj /pg check [Gracz] lub /pg check" ));							
		PlayerInvalid = ChatColor.translateAlternateColorCodes('&',message.getString("Message.PlayerInvalid", "Nie znaleziono gracza " ));							
		Permissions = ChatColor.translateAlternateColorCodes('&',message.getString("Message.Permissions", "You do not have permission!" ));	
		NoCommands = ChatColor.translateAlternateColorCodes('&',message.getString("Message.NoCommands", "Brak komendy wpisz /MowPing" ));
		
		Admins = message.getStringList("Admins");
	}
}

package net.myotherworld.Ping.Data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigData
{
	public int Max;
	public List<String> Commands;
	public List<String> WarringCommands;
	public long Timer;
	public int Loop;
	public List<String> enabledWorlds = new ArrayList<String>();
	
	public ConfigData(YamlConfiguration config) 
	{
		enabledWorlds.clear();
		enabledWorlds = config.getStringList("Worlds");	
		
		Timer = config.getInt("Timer", 200 );
		Max = config.getInt("Max", 500 );
		WarringCommands = config.getStringList("WarringCommands");
		Commands = config.getStringList("Commands");
		Loop = config.getInt("Loop", 3);
	}
}

package net.myotherworld.Ping.Commands;


import java.io.File;

import net.myotherworld.Ping.Ping;
import net.myotherworld.Ping.Data.ConfigData;
import net.myotherworld.Ping.Data.MessageData;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AdminCommand implements CommandExecutor
{
	private Ping plugin;
	
	public AdminCommand(Ping plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender.hasPermission("MyOtherWorldPing.admin")) 
		{		
			if (args.length > 0) 				
			{			
				if(args[0].equalsIgnoreCase("Reload"))
				{	    
					if (args.length == 2) 				
					{
						if(args[1].equalsIgnoreCase("Config"))
						{
							if(sender.hasPermission("MyOtherWorldPing.admin.reload.config")) 
							{
								plugin.config = new ConfigData(plugin.fileManager.loadConfig());
								plugin.Scheduler.cancel();
								plugin.Scheduler();
						        sender.sendMessage("Poprawnie przeladowano config.yml.");
						        return true;
							}
						}
						else if(args[1].equalsIgnoreCase("Msg"))
						{
							if(sender.hasPermission("MyOtherWorldPing.admin.reload.message")) 
							{
								plugin.message = new MessageData(plugin.fileManager.loadMessages());
						        sender.sendMessage("Poprawnie przeladowano message.yml.");
						        return true;
							}	
						}
						else if(args[1].equalsIgnoreCase("all"))
						{
							if(sender.hasPermission("MyOtherWorldPing.admin.reload.all")) 
							{
								plugin.config = new ConfigData(plugin.fileManager.loadConfig());
								plugin.Scheduler.cancel();
								plugin.Scheduler();
								plugin.message = new MessageData(plugin.fileManager.loadMessages());
								sender.sendMessage("Poprawnie przeladowano wszystkie Cfg.");
								return true;
							}
						}
						else
						{
							sender.sendMessage(plugin.message.Prefix + plugin.message.NoCommands);
							return true;
						}	
					}
					for(String m : plugin.message.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.message.Prefix + m);						
					}
					return true;
				}
				else if(args[0].equalsIgnoreCase("Clear"))
				{	
					if(sender.hasPermission("MyOtherWorldPing.admin.clear")) 
					{
						plugin.Scheduler.cancel();
				        File file = new File(plugin.getDataFolder(), "user.yml");
				        if(file.delete()) 
				        {
				        	sender.sendMessage(plugin.message.Prefix + "Successfully deleted: user.yml!");
				        }
				        else 
				        {
				            sender.sendMessage(plugin.message.Prefix + "Failed to delete file: user.yml");
				        }
				        plugin.fileManager.loadUser();
						plugin.Scheduler();
						return true;
					}
					else
					{
						sender.sendMessage(plugin.message.Prefix + plugin.message.NoCommands);
						return true;
					}
				}
				else
				{
					for(String m : plugin.message.Admins) 
					{
						m = ChatColor.translateAlternateColorCodes('&', m);
						sender.sendMessage(plugin.message.Prefix + m);					
					}
					return true;
				}
			}
			else
			{
				for(String m : plugin.message.Admins) 
				{
					m = ChatColor.translateAlternateColorCodes('&', m);
					sender.sendMessage(plugin.message.Prefix + m);				
				}	
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.message.Prefix + plugin.message.Permissions);
		}
		return true;
	}
}

package net.myotherworld.Ping.Commands;

import net.myotherworld.Ping.Ping;
import net.myotherworld.Ping.Utils;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PingCommand implements CommandExecutor
{
	private Ping plugin;
	public PingCommand(Ping plugin)
	{
		this.plugin = plugin;
	}	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) 
	{
		if (args.length > 0) 				
		{
			if(args[0].equalsIgnoreCase("Check"))
			{	
		    	if(sender.hasPermission("MyOtherWorldPing.check")) 
		    	{ 
		    		if (sender instanceof Player) 
		    		{
						if(args.length == 1)
						{
							Player p = (Player)sender;
							int ping = Utils.getPlayerPing(p);	
		                	sender.sendMessage(plugin.message.Prefix + plugin.message.Ping +" "+ String.valueOf(ping) + "ms");
							return true;
						}				
		    		}
					else
					{
		    			sender.sendMessage("Tylko gracz moze tego uzyc!");
		    			return true;		
					}	

					if ((args.length == 2) && ((sender.hasPermission("MyOtherWorldPing.admin") || (sender.hasPermission("MyOtherWorldPing.check.others"))))) 				
					{	
						Player targetPlayer = Bukkit.getServer().getPlayer(args[1]);	
									
		                if (targetPlayer == null)
		                {
		                	sender.sendMessage(plugin.message.Prefix + plugin.message.PlayerInvalid + args[1]);
		                    return true;
		                }
		                else
		                {
		                	int ping = Utils.getPlayerPing(targetPlayer);	
		                	sender.sendMessage(plugin.message.Prefix + plugin.message.Ping + args[1] +" "+ String.valueOf(ping) + "ms");
		                }
		                
					}
					else
					{
						sender.sendMessage(plugin.message.Prefix + plugin.message.Check);
					}
		    	}
				else
				{
					sender.sendMessage(plugin.message.Prefix + plugin.message.Permissions);
				}
				return true;
			}
		}
		else
		{
			sender.sendMessage(plugin.message.Prefix + plugin.message.Check);
		}
		return true;
		}
}
    
